---
title: "Andrea"
image: "images/author/andrea.png"
email: "andrea.negri@gmail.com"
date: 2021-02-01T10:14:19+06:00
draft: false
social:
- icon: "la-facebook-f"
  link: "https://facebook.com/nigerpunk"
- icon: "la-twitter"
  link: "https://twitter.com/nigerpunk"
---

Sono Andrea, (quasi) quarantenne, papà, marito, smartworker in tempi non sospetti pre-pandemia. Questo sito è una mia riflessione “a voce alta” sull’acquisto di una cargo bike per sostituire una delle due automobili che abbiamo in casa attualmente.