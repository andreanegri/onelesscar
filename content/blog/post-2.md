---
title: "I requisiti di una buona cargo bike"
image: "images/post/02-title.jpg"
date: 2022-02-23T20:19:25+06:00
author: "Andrea Negri"
tags: []
categories: []
draft: false
---

Da neofita, ma con motivazioni che cercherò di giustificare in questo articolo, ho fatto varie considerazioni sulle caratteriste della cargo bike "ideale", almeno per quelle che credo siano le mie esigenze attuali, e ho individuato queste categorie:

- tipologia
- volume di carico
- alimentazione
- peso

### Tipologia

Esistono cargo bike di diverse categorie, la rete è ricca di informazioni interessanti.  
Qui qualche lettura integrativa: [questa](https://momentummag.com/a-primer-on-the-different-styles-of-cargo-bike/) e [quest'altra](https://bikeshopgirl.com/cargo-bike-guide/).

Personalmente, vorrei una esperienza di guida vicina all'uso di una bicicletta classica, e ho deciso di concentrare le mie ricerche su cargo bike di tipo **long john**, mezzi a due ruote con zona di carico anteriore.<br>
<img style="display: block; margin: 0 auto; padding-top: 20px; padding-bottom: 20px;" src="../../images/post/02-long.jpg"></img>

Nella parte anteriore può essere installato un pianale, per appoggiare bagagli vari, oppure un vero e proprio _cassone_, che viene configurato in base alle esigenze: dall'alloggiamento di un sedile per i bimbi a box richiudibili per caricare merce.

<div style="display: table; margin: 0 auto;">
<img style="padding: 5px" src="../../images/post/02-kids.jpg"></img>
<img src="../../images/post/02-load.jpg"></img>
</div>

### Volume di carico

Visto che la mia idea è sostituire l'automobile con una cargo, vorrei un mezzo che mi permetta di portare mia figlia Agnese (2 anni e mezzo al momento) e un pò di spesa in contemporanea.  
Al momento, quando non voglio usare la macchina e andiamo a fare la spesa, sono organizzato con la mia **MTB con seggiolino anteriore**, **portapacchi** e **zaino per portapacchi da 25L** ([questo](https://www.decathlon.it/p/zaino-per-portapacchi-bici/_/R-p-300851?mc=8405450&gclid=CjwKCAiAgbiQBhAHEiwAuQ6BkmAlnPY-R0eZrr1NVG1We0RUd51i13yF5K4B7U8DFSn6Ll9RrE2XxBoCLkIQAvD_BwE) di Decathlon, sono molto soddisfatto), che mi permettono di prendere un pochino di roba, ma niente di troppo voluminoso a meno di non voler fare un _tetris_ per caricare tutto.  
I problemi sono principalmente due: ormai Agnese davanti sta stretta (infatti normalmente la porto in giro nel seggiolino posteriore della mia ciclocross) e se volessi, che so, prendere un pacco d'acqua o una cassetta di mele non saprei come fare.

Una cargo risolverebbe il problema: c'è spazio per caricare tutto, sia in volume che in peso massimo consentito: si parla di almeno 50-100kg caricabili, oltre al guidatore.

Una cosa che ho notato è che nessun produttore mostra distintamente lo spazio di carico disponibile nel cassone anteriore. Fortunatamente qualcuno prima di me ci ha pensato: **Cargo Bike Magazine** ha postato un [articolo](https://cargobikemag.com/cargo-space-the-overview/) in cui sono fotografati, dall'altro, i cassoni di molte cargo in commercio, con dimensioni indicate in modo chiaro.<br>
Vedere, una dietro l'altra, le foto di tanti mezzi diversi aiuta a rendersi conto delle differenze tra uno e l'altro. 

In futuro mi sono ripromesso di fare **un post ad hoc di confronto tra le dimensioni dei vani di carico**; mentre aspettate c'è [questo articolo](https://www.colchester.gov.uk/ecargobikelibrary/?id=&page=photographs--of--colchester%27s--ecargo--bikes) che mostra quanto si possano caricare alcune delle più famose cargo in commercio usando come riferimento degli scatoloni di cartone.

### Alimentazione

La scelta è tra una cargo bike muscolare :muscle: oppure una dotata di assistenza elettrica :zap:.  
Le prime hanno un costo inferiore e un peso ridotto grazie all'assenza del gruppo motore: nel mio caso, visto che sto pensando a un _car-replacement_, la risposta è piuttosto scontata: **E-Cargo tutta la vita**.  
E' vero che per le mie esigenze dovrò spostarmi quasi sempre nell'ordine di qualche km, al massimo una decina, ma non voglio precludermi la possibilità di andarci in ufficio (circa 45km da casa) o sentirmi limitato negli spostamenti più lunghi per poi rinunciare e lasciare la cargo parcheggiata a casa.  
Altro aspetto: la moglie non gradisce tanto pedalare, ma quando in una recente vacanza in Trentino abbiamo noleggiato due E-bike, l'ho vista abbastanza gasata con la **modalità Turbo** perennemente attiva :smile:.  
_Va a finire che lei usa la cargo e io giro in macchina..._

Quando si parla di assistenza, arriva sempre il momento in cui ci si chiede:

> Ma la batteria? Quanto dura?

Al di là delle valutazioni più o meno soggettive, Bosch ha un [simulatore](https://www.bosch-ebike.com/it/assistenza/assistente-autonomia) molto carino per rispondere a questa domanda, che tiene conto di diversi fattori, come la tipologia di bici, il tipo di motore, il percorso, il peso di bici+ciclista, il livello di assistenza desiderato, ecc.  
Per l'uso che ne dovrò fare, penso che una batteria da 400-500Wh consenta i classici spostamenti quotidiani sempre in modalità Turbo senza _lasciarmi a piedi_.

### Peso

Il peso influisce su tante cose: la maneggevolezza del mezzo, la difficoltà nel pedalare per tragitti medio-lunghi, la stabilità e sicurezza.
Mentre per quanto riguarda il peso massimo non ci sono particolari limiti, si trovano cargo bike che arrivano a superare i 45-50kg di peso, cercando le più leggere sono rimasto stupito.  
Tra le muscolari, sicuramente la [Bullit](https://www.larryvsharry.com/) ha fatto breccia nel mio cuore: si parte da modelli di nemmeno 23kg, che permettono una guida sportiva e divertente.  
Tra le elettriche, ho visto che la **Packster 40** di **Riese&Muller** sta poco sotto i 30kg, incredibile!  
Ovviamente questa leggerezza ha un prezzo: la P40 è lunga _solo_ 2.23 metri, di fatto una "utilitaria" nel mondo cargo bike! :smile:

### Conclusioni

Bene, una volta individuati i punti cardine su cui porre attenzione, non resta che iniziare a valutare qualche modello di cargo bike e capire quanto soddisfi le mie esigenze! Nei prossimi articoli vi racconto quale sarà il mezzo da sogno su cui puntare! :smile:

----

<sub>Illustrazione della long john di Robert Higdon</sub>