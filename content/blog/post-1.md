---
title: "Si parte!"
image: "images/post/01-title.jpg"
date: 2022-02-01T18:19:25+06:00
author: "Andrea Negri"
tags: []
categories: []
draft: false
---

### Ci siamo

Con questo primo post, parte ufficialmente il progetto **One Less Car**, OLC per gli amici.<br>
Potete leggere due parole sul progetto [qui]({{< ref "/olc" >}} "OLC").

Ho pure fatto la grafica per gli sticker, brutta :sweat_smile:.

![image](../../images/post/01-sticker.png)

Di cosa parleremo? Vi racconterò la mia avventura alla ricerca della cargo bike "perfetta" per le esigenze della nostra famiglia, di come mi troverò, delle gioie e dei dolori che porteranno questa scelta un pò matta, un pò desiderata da tempo.<br>
Forse alcuni (tutti?) articoli saranno noiosi perchè sono più note scritte di miei pensieri e idee più che storie da raccontare agli altri, ma chi lo sa, magari potrebbero tornare utili a qualcuno, e li pubblicherò lo stesso :wink:.

### Qualche tecnicismo

Per l'_angolo del nerd_ :nerd:, alcune info sul sito:
- è realizzato con il framework [Hugo](https://gohugo.io/)
- è hostato sulla piattaforma Gitlab, [qui](https://gitlab.com/andreanegri/onelesscar) trovate i sorgenti se volete dare una sbirciatina
- il tema è il bellissimo, almeno per me, [Bookworm light](https://github.com/gethugothemes/bookworm-light) :bug:

Ho scelto Hugo perchè sono uno sviluppatore software, e modificare file _Markdown_ con _Visual Studio Code_ mi risulta più naturale rispetto ad usare editor e client vari (`WordPress`, ti voglio bene comunque :heart:).<br>
Posso modificare in locale i file nel _repository git_ del progetto, poi con un `push` il sito viene rigenerato automaticamente e i post e le pagine modificate/aggiunte **magicamente** finiscono al loro posto: troppo comodo!<br>
Altra cosa simpatica, se un domani volessi cambiare il tema, i contenuti degli articoli sono in file di testo facilmente modificabili, se fosse necessario.

Ma ora basta roba da geek, qui si parla di **cargo bike**.<br>
Alla prossima!

----

<p>
<sub>Credits: title image by <a href="https://pixabay.com/users/geralt-9301/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=5896299" target="_blank">Gerd Altmann</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=5896299" target="_blank">Pixabay</a></sub>
</p>