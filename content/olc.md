---
title: "OLC
"
layout: "about"
image: "images/cargo-bw.jpg"
draft: false

#caption
caption:
  enable: true
  title: "One Less Car, una cargo alla volta"

# social
social:
  enable: true
  social_links:
  - link: "https://www.facebook.com/nigerpunk"
    icon: "lab la-facebook-f"

  - link: "https://www.twitter.com/nigerpunk"
    icon: "lab la-twitter"

  - link: "https://www.instagram.com/onemorelesscar"
    icon: "lab la-instagram"

# what_i_do
what_i_do:
  title: "What I Do"
  enable: false
  item:
  - title: "Content Writing"
    icon: "las la-pen-nib"
    description: "Purus eget ipsum elementum venenatis, quis rutrum mi semper nonpurus eget ipsum elementum venenatis."
    
  - title: "Photography"
    icon: "las la-camera"
    description: "Aenean maximus urna magna elementum venenatis, quis rutrum mi semper non purus eget ipsum elementum venenatis."
    
  - title: "Web Research"
    icon: "lar la-snowflake"
    description: "Aenean maximus urna magna elementum venenatis, quis rutrum mi semper non purus eget ipsum elementum venenatis."
 
---

#### Incipit

Le *cargo bike* mi hanno sempre affascinato. Vi state chidendo cosa sia una cargo bike? Citando [wikipedia](https://it.wikipedia.org/wiki/Bicicletta_da_trasporto) :mortar_board::

> Le Biciclette da trasporto (note anche come bici cargo o cargo bike) sono delle biciclette o velocipedi progettati e costruiti specificatamente per il trasporto persone, animali e merci.

Mi danno l'idea di libertà, di mezzo che aggrega, adatto al viaggiatore solitario così come alla famiglia che si muove.

In più sono anche **ingombranti**, nel senso buono del termine, e nel traffico probabilmente ti fanno sentire meno alla mercè delle automobili. Ma partiamo dall'inizio...


#### Capitolo Primo: 2008 - 2019

Gli undici anni del titolo sono quelli che ho passato a fare il pendolare tra la mia residenza, cambiata nel corso del tempo varie volte, ma sempre nella provincia di **Modena**, e la mia sede di lavoro, in provincia di **Bologna**. Tratta tutto sommato breve, poco meno di 50km, ma che ho coperto il 99,9% delle volte in auto. Il restante 0,1% mi sono mosso in bicicletta (ma si contano sulle dita di due mani) o con i mezzi (si contano sulle dita di una mano a cui abbiano amputato un paio di dita...).

La tabella sotto mostra una comparativa dei tempi di percorrenza nel tragitto casa-ufficio.

| Mezzo di trasporto  | Tempo a tratta (minuti) |
|:-------------:|:-------------:| 
| :car:            | 45-60  |
| :bike:           | 90-100 | 
| :bus: / :train:  | 120    |

Commentiamo un pò i numeri. Non sono un fan dei mezzi pubblici, lo ammetto, ma 4 ore al giorno per fare 100km sono davvero troppi, e ho sempre scartato categoricamente la possibilità, pur essendo economicamente più vantaggiosa di altre. 

Curiosamente andare in ufficio in bicicletta è più veloce, ma presenta alcuni lati negativi:
- poco fattibile nel quotidiano, a causa delle strade da fare, alcune davvero trafficate e con mezzi che corrono ad alta velocità (l'alternativa sarebbe allungare in maniera drastica il percorso)
- a livello logistico bisogna considerare che, arrivati al lavoro dopo 50km a discreta andatura (se non si vuole aumentare troppo il tempo), una doccia è indispensabile, e porta via altro tempo
- nei mesi invernali si pedala sempre al buio, sempre sulle suddette strade tendenzialmente pericolose

In sostanza, un pò per pigrizia, un pò per comodità, sono sempre stato un sostenitore del _team automobile_.

#### Capitolo Secondo: :baby_bottle: Agnese :baby_bottle: (2019)

Il 2019 è un anno importante per me: a settembre la nostra famiglia si è allargata con l'arrivo della piccola **Agnese**, una biondina tutto pepe che ha rapito dal primo secondo il cuore di mia moglie e il mio.
La nascita di Agnese è coincisa con la mia richiesta di lavorare da remoto con un rientro settimanale in ufficio. Fortunatamente la mia tipologia di lavoro (sono uno sviluppatore software senior), sommata al fatto che l'azienda per cui lavoro è abbastanza _avanti_, ha fatto sì che si potesse realizzare il mio desiderio.<br>
Dopo un iniziale periodo di ambientamento, nel quale ho dovuto imparare a ritagliarmi uno spazio di lavoro in un contesto domestico, riuscire a bilanciare i tempi di lavoro con quelli di vita quotidiana, devo dire che ho trovato la mia dimensione.

Vivendo in una città di provincia, con tutti i servizi abbastanza vicini, recuperare il tempo di viaggio casa-lavoro e lavoro-casa vuol dire riuscire a fare cose che prima erano impensabili: andare a fare la spesa in pausa pranzo, potere cucinare il pranzo per la propria famiglia, portare la bimba dal pediatra in piena mattina in caso di emergenza, presentarsi puntuale alla partita di padel (sì, anche io sono entrato in fissa col padel, _non giudicatemi_) fissata alle 18.30, eccetera.<br>
Questo cambio di routine mi ha fatto riflettere: potevo lasciare parcheggiata l'automobile molto più tempo, perchè di fatto per le piccole commissioni giornaliere potevo spostarmi a piedi o in bicicletta. Servirsi solo della bici è possibile? _- Eh, però il giorno che devo andare in ufficio come faccio? -_ Il pendolarismo tornava sempre fuori a tarparmi le ali... Ho iniziato a pensare che, magari, avrei potuto comprare una **ebike** da affiancare all'automobile, e usare quella per spostarmi nelle tratte più lunghe. Si potrebbero pure fare delle gite in bicicletta, la bici elettrica la affido alla moglie che non è molto contenta di faticare sui pedali, mentre io e la bimba andiamo sulla muscolare.

#### Capitolo Terzo: :mask: Pandemia :mask: (2020 - ...)

Al momento in cui scrivo, febbraio 2022, non siamo ancora usciti dal turbinìo del Covid-19. La pandemia, a livello lavorativo, e se proprio vogliamo vedere il bicchiere mezzo pieno, mi ha _regalato_ il lavoro completamente da remoto. Ad oggi sono tornato in ufficio una volta in due anni per prendere alcune cose, per il resto lavoro da casa full-time. La mia azienda ha definito un regolamento per lo smartworking che permetterà il lavoro da remoto anche al termine dell'emergenza: questo per me vorrebbe dire potere comunque lavorare da casa per lunghi periodi e potere ad esempio andare in ufficio solo quando fosse strettamente necessario, e comunque in modo organizzato e pianificato.
Grazie a questa _congiunzione astrale_ nasce il progetto **One Less Car**: vendere una delle due auto di casa (sono onesto, al momento fatico a immaginare una nostra vita famigliare completamente senza automobile) e sostituirla con un mezzo alternativo, una **cargo bike** (sono stato veloce ad arrivare al punto, eh?), possibilmente **elettrica**.

Perchè una cargo bike?

- :rainbow: pedalare mi dà felicità
- per far provare ad Agnese esperienze felici da ricordare quando sarà più grande (_- Grazie papà per avermi portato all'asilo con la neve a -2°C nel cassone gelato di quel biciclettone! -_ :stuck_out_tongue:)
- posso andare sulle piste ciclabili: è comunque una bicicletta, sebbene XXL! :smile:
- posso farci una spesa medio-grossa (es. caricare pacchi d'acqua)
- posso ovviamente portare Agnese + un altro bimbo o due in giro (da orgoglioso figlio unico non sto pensando di allargare la famiglia, ma mia moglie è la quinta di cinque fratelli, quindi... :sweat_smile:)
- posso portare in giro _Agnese + la spesa_ contemporaneamente, cosa da non sottovalutare
- :umbrella: :snowflake: :zap: in caso di brutto tempo posso tenere la piccola al coperto con cappottine e quant'altro, esistono molti accessori, soprattutto per i marchi più blasonati, se non si è fan del DIY
- :seedling: :herb: impatto ambientale minore rispetto all'automobile

In più, con un occhio al portafoglio:

- :money_with_wings: niente bollo (nel mio caso, circa 150-170€ risparmiati ogni anno)
- :money_with_wings: niente assicurazione (400-600€ in meno)
- :money_with_wings: costo di manutenzione sicuramente inferiore

Visto che sto pensando a un _car-replacement_, mi sto orientando su una cargo bike **elettrica**: non mi spaventa pedalare, ma quando servisse quel livello di assistenza in più, non mi dispiacerebbe l'idea di averlo a disposizione, magari quando piove e sei carico come un mulo.

Se siete arrivati in fondo a questo sproloquio, e ve lo state chiedendo, cosa troverete su questo sito?<br>
Le mie esperienze sulla scelta, l'acquisto, i feedback, gioie e dolori dell'uso di una cargo bike.

##### Vi aspetto, se vi va! :blush:
